//
//  AppDelegate.swift
//  Notification_actions_Demo
//
//  Created by vignesh kumar c on 09/08/23.
//

import UIKit
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
               requestNotificationAuthorization()
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if settings.authorizationStatus == .authorized {
                self.scheduleNotificationWithActions()
            }
        }
        return true
    }
    
    func requestNotificationAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if granted {
                self.setupNotificationCategories()
                self.scheduleNotificationWithActions()
            } else {
                // Handle permission denied
                print("Permission not granted")
            }
        }
    }
    
    func setupNotificationCategories() {
        let action1 = UNNotificationAction(identifier: "action1", title: "Action 1", options: [])
        let action2 = UNNotificationAction(identifier: "action2", title: "Action 2", options: [])
        
        let category = UNNotificationCategory(identifier: "myCategory", actions: [action1, action2], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
          if response.actionIdentifier == "action1" {
              // Handle Action 1
//              handleActions1(notification: response.notification)
              print("Action 1 was selected")
          } else if response.actionIdentifier == "action2" {
              // Handle Action 2
//              handleActions1(notification: response.notification)
              print("Action 2 was selected")
          }

          completionHandler()
      }
    
    func scheduleNotificationWithActions() {
          let content = UNMutableNotificationContent()
          content.title = "My Notification"
          content.body = "This is a notification with actions."
          content.categoryIdentifier = "myCategory"

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
          let request = UNNotificationRequest(identifier: "myNotification", content: content, trigger: trigger)

          UNUserNotificationCenter.current().add(request) { error in
              if let error = error {
                  print(error)
              }
          }
      }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           // Customize the presentation options based on your preference
           completionHandler([.banner, .sound]) // Display banner and play sound
       }
    
    func handleActions1(notification: UNNotification) {
        let content = notification.request.content
        print(content.title)
    }
    
    func handleActions2(notification: UNNotification) {
        let content = notification.request.content
        print(content.title)
    }
}

