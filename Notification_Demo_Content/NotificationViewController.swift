//
//  NotificationViewController.swift
//  Notification_Demo_Content
//
//  Created by vignesh kumar c on 09/08/23.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = ""
    }
    
    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        print("Didreceive called")
        print("Action Identifier:\(response.actionIdentifier)")
        if response.actionIdentifier == "action1" {
            if let textResponse = response as? UNTextInputNotificationResponse {
                let reply = textResponse.userText
            }
        } else if response.actionIdentifier == "action2" {
            if let textResponse = response as? UNTextInputNotificationResponse {
                let reply = textResponse.userText
            }
        }
        completion(.dismissAndForwardAction)
    }
}
